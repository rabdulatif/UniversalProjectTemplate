﻿using Microsoft.AspNetCore.Http;
using System.Net;

namespace Root.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class HttpContextHelper
    {
        /// <summary>
        /// 
        /// </summary>
        public static IHttpContextAccessor Accessor;

        /// <summary>
        /// 
        /// </summary>
        public static HttpContext Current => Accessor?.HttpContext;

        /// <summary>
        /// 
        /// </summary>
        public static void SetErrorInBodyCode()
        {
            SetStatusCode(HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        public static void SetStatusCode(HttpStatusCode code)
        {
            if (Current?.Response != null)
                Current.Response.StatusCode = (int)code;
        }
    }
}
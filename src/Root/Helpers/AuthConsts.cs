﻿namespace Root.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class AuthConsts
    {
        /// <summary>
        /// 
        /// </summary>
        public const int EXPIRE_MINUTES = 60;

        /// <summary>
        /// 
        /// </summary>
        public const int EXPIRE_MINUTES_REFRESH = 200;

        /// <summary>
        /// 
        /// </summary>
        public const string PARAM_ISS = "HostAuth";

        /// <summary>
        /// 
        /// </summary>
        public const string PARAM_AUD = "up_proj";

        /// <summary>
        /// 
        /// </summary>
        public const string PARAM_SECRET_KEY = "Y2V0Y2hldiUyMZdvbmclBjBsb3ZlJTIwLm5ldA==";

        /// <summary>
        /// 
        /// </summary>
        public const int MAX_DEVICE_COUNT = 1;
    }
}
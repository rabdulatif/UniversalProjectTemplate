﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Root.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class ClientDeviceHelper
    {
        private static IHttpContextAccessor _accessor = HttpContextHelper.Accessor;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static string GetIpAndUserAgent(string separator) => GetRequestIP() + separator + GetUserAgent();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tryUseXForwardHeader"></param>
        public static string GetRequestIP(bool tryUseXForwardHeader = true)
        {
            try
            {
                string ip = null;

                if (tryUseXForwardHeader)
                    ip = GetHeaderValueAs<string>("X-Forwarded-For").SplitCsv().FirstOrDefault();

                if (ip.INOWS() && _accessor.HttpContext.Connection.RemoteIpAddress != null)
                    ip = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();

                if (ip.INOWS())
                    ip = GetHeaderValueAs<string>("REMOTE_ADDR");

                return ip;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static string GetUserAgent()
            => HttpContextHelper.Current.Request.Headers.ContainsKey("User-Agent")
                ? HttpContextHelper.Current.Request.Headers["User-Agent"]
                : string.Empty;

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="headerName"></param>
        /// <returns></returns>
        private static T GetHeaderValueAs<T>(string headerName)
        {
            StringValues values = string.Empty;

            if (_accessor.HttpContext?.Request?.Headers?.TryGetValue(headerName, out values) ?? false)
            {
                string rawValues = values.ToString();

                if (!rawValues.INOWS())
                    return (T)Convert.ChangeType(values.ToString(), typeof(T));
            }
            return default;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="csvList"></param>
        /// <param name="nullOrWhitespaceInputReturnsNull"></param>
        /// <returns></returns>
        private static List<string> SplitCsv(this string csvList, bool nullOrWhitespaceInputReturnsNull = false)
            => csvList.INOWS()
                ? (nullOrWhitespaceInputReturnsNull ? null : new List<string>())
                : csvList.TrimEnd(',').Split(',').AsEnumerable().Select(s => s.Trim()).ToList();
    }
}

﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Root.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class EmbeddedResource
    {
        /// <summary>
        /// </summary>
        /// <param name="assembly"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static StreamReader GetStream(Assembly assembly, string name)
        {
            foreach (var resName in assembly.GetManifestResourceNames())
                if (resName.EndsWith(name))
                    return new StreamReader(assembly.GetManifestResourceStream(resName) ??
                                            throw new InvalidOperationException());
            return null;
        }

        /// <summary>
        /// </summary>
        /// <param name="assembly"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetString(Assembly assembly, string name)
        {
            var sr = GetStream(assembly, name);
            var data = sr.ReadToEnd();
            sr.Close();
            return data;
        }

        /// <summary>
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetString(string name)
        {
            return GetString(typeof(EmbeddedResource).Assembly, name);
        }

        /// <summary>
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        public static string[] GetNames(string folder = null)
        {
            if (folder == null)
                return typeof(EmbeddedResource).Assembly.GetManifestResourceNames();

            return typeof(EmbeddedResource).Assembly.GetManifestResourceNames().Where(w => w.StartsWith(folder))
                .ToArray();
        }
    }
}
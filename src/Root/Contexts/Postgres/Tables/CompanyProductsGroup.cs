﻿namespace Root.Contexts.Postgres.Tables
{
    /// <summary>
    /// 
    /// </summary>
    public class CompanyProductsGroup
    {
        /// <summary>
        /// 
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int ProductCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int  TotalSum { get; set; }
    }
}
﻿using Root.Contexts.Postgres.Tables.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Root.Contexts.Postgres.Tables
{
    /// <summary>
    /// 
    /// </summary>
    [Table("users")]
    public class User : BaseEntity
    {
        /// <summary>
        /// 
        /// </summary>
        [Column("firstname", TypeName = "varchar(50)")]
        public string FirstName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("lastname", TypeName = "varchar(50)")]
        public string LastName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("middlename", TypeName = "varchar(50)")]
        public string MiddleName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("username", TypeName = "varchar(20)")]
        public string Username { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("hash_password")]
        public string HashPassword { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("salt")]
        public string Salt { get; set; }
    }
}
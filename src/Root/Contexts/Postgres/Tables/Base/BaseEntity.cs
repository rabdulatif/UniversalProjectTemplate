﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Root.Contexts.Postgres.Tables.Base
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// 
        /// </summary>
        [Column("id")]
        public long Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column("created_on")]
        public DateTime CreatedOn { get; set; } = DateTime.Now;

        /// <summary>
        /// 
        /// </summary>
        [Column("modified_on")]
        public DateTime ModifiedOn { get; set; } = DateTime.Now;

        /// <summary>
        /// 
        /// </summary>
        [Column("is_deleted")]
        public bool IsDeleted { get; set; } = false;
    }
}
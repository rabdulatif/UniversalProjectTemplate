﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Root.Contexts.Postgres.Tables
{
    /// <summary>
    /// 
    /// </summary>
    [Table("bot_variables")]
    public class BotVariable
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }
    }
}
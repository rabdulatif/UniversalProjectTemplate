﻿using System.Collections.Generic;

namespace Root.Contexts.Postgres.Tables
{
    /// <summary>
    /// 
    /// </summary>
    public class Company
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<Product> Products { get; set; } = new List<Product>();
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Root.Contexts.Postgres.Tables;

namespace Root.Contexts.Postgres
{
    /// <summary>
    /// 
    /// </summary>
    public partial class UPContext
    {
        /// <summary>
        /// 
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<BotVariable> Variables { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<CompanyProductsGroup> ProductsByCompany { get; set; }
    }
}
﻿using Microsoft.EntityFrameworkCore;

namespace Root.Contexts.Postgres
{
    /// <summary>
    /// 
    /// </summary>
    public partial class UPContext : DbContext
    {
        /// <summary>
        /// 
        /// </summary>
        public static string ConnectionString { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        public UPContext(DbContextOptions<UPContext> options) : base(options) { }

        /// <summary>
        /// 
        /// </summary>
        public UPContext() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(ConnectionString);
        }
    }
}
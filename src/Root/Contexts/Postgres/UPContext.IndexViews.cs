﻿using Microsoft.EntityFrameworkCore;
using Root.Contexts.Postgres.Tables;

namespace Root.Contexts.Postgres
{
    /// <summary>
    /// 
    /// </summary>
    public partial class UPContext
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Indexes

            modelBuilder.Entity<User>().HasIndex(o => o.Username).IsUnique();
            modelBuilder.Entity<User>().HasIndex(o => o.FirstName);

            #endregion

            #region Views

            modelBuilder.Entity<CompanyProductsGroup>().HasNoKey();

            #endregion

        }
    }
}
CREATE
OR REPLACE
VIEW View_ProductsByCompany AS 
    SELECT c.Name AS CompanyName, Count(p.Id) AS ProductCount, Sum(p.Price * p.TotalCount) AS TotalSum
    FROM Companies c
    INNER JOIN Products p on p.CompanyId = c.Id
    GROUP BY c.Name
﻿namespace System
{
    /// <summary>
    /// 
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string SafeLowTrim(this string value)
            => string.IsNullOrWhiteSpace(value) ? string.Empty : value.ToLower().Trim();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string SafeTrim(this string value) 
            => string.IsNullOrWhiteSpace(value) ? string.Empty : value.Trim();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool INOWS(this string value) => string.IsNullOrWhiteSpace(value);
    }
}
﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Root.Helpers;
using System;
using System.Text;
using System.Threading.Tasks;

namespace Root.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class JwtConfigurationExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        public static void ConfigureJwtAuthService(this IServiceCollection service)
        {
            var keyByteArray = Encoding.ASCII.GetBytes(AuthConsts.PARAM_SECRET_KEY);
            var signingKey = new SymmetricSecurityKey(keyByteArray);

            var tokenValidationParameters = GetTokenValidationParameters(signingKey);

            service.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(o =>
                {
                    o.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            var accessToken = context.Request.Query["access_token"];

                            var path = context.HttpContext.Request.Path;
                            if (!string.IsNullOrEmpty(accessToken) && (path.StartsWithSegments("/notification")))
                                context.Token = accessToken;

                            return Task.CompletedTask;
                        }
                    };

                    o.TokenValidationParameters = tokenValidationParameters;
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="signingKey"></param>
        /// <returns></returns>
        private static TokenValidationParameters GetTokenValidationParameters(SymmetricSecurityKey signingKey)
        {
            var validationParameters = new TokenValidationParameters();
            validationParameters.ValidateIssuerSigningKey = true;
            validationParameters.IssuerSigningKey = signingKey;

            validationParameters.ValidateIssuer = true;
            validationParameters.ValidIssuer = AuthConsts.PARAM_ISS;

            validationParameters.ValidateAudience = true;
            validationParameters.ValidAudience = AuthConsts.PARAM_AUD;

            validationParameters.ValidateLifetime = true;

            validationParameters.ClockSkew = TimeSpan.Zero;

            return validationParameters;
        }
    }
}

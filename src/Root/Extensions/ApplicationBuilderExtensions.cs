﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Root.Helpers;
using Root.Logics.Middlewares;
using Root.Logics.Middlewares.Languages;

namespace Root.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class ApplicationBuilderExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public static void UseLanguageMiddleware(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<LanguageMiddleware>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseRequestResponseLogging(this IApplicationBuilder app)
        {
            app.UseMiddleware<RequestResponseLoggingMiddleware>();
            return app;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TContext"></typeparam>
        /// <param name="app"></param>
        public static void UseMigrateDatabase<TContext>(this IApplicationBuilder app) where TContext : DbContext
        {
            var factory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var context = factory.ServiceProvider.GetService<TContext>();
            context?.Database?.Migrate();

            ExecuteDatabaseViews(context?.Database);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="database"></param>
        private static void ExecuteDatabaseViews(DatabaseFacade database)
        {
            foreach (var name in EmbeddedResource.GetNames("Root.SqlScripts.Views"))
                database?.ExecuteSqlRaw(EmbeddedResource.GetString(name));
        }
    }
}
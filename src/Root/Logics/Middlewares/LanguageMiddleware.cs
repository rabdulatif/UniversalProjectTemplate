﻿using Microsoft.AspNetCore.Http;
using SB.Common.Logics.SynonymProviders;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace Root.Logics.Middlewares.Languages
{
    /// <summary>
    /// 
    /// </summary>
    public class LanguageMiddleware
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly Dictionary<string, string> _languages;

        /// <summary>
        /// 
        /// </summary>
        private readonly RequestDelegate _next;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="next"></param>
        public LanguageMiddleware(RequestDelegate next)
        {
            _next = next;

            _languages = new Dictionary<string, string>
            {
                { "en", CultureHelper.EnLanguageName },
                { "uz", CultureHelper.UzLanguageName },
                { "ru", CultureHelper.RuLanguageName }
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task InvokeAsync(HttpContext context)
        {
            CultureInfo.CurrentCulture  = CultureInfo.GetCultureInfo(CultureHelper.RuLanguageName);
            CultureInfo.CurrentUICulture=CultureInfo.GetCultureInfo(CultureHelper.RuLanguageName);

            var language = context.Request.Headers["Language"];
            if (!string.IsNullOrEmpty(language) && _languages.TryGetValue(language, out var cultureName))
            {
                CultureInfo.CurrentCulture = CultureInfo.GetCultureInfo(cultureName);
                CultureInfo.CurrentUICulture = CultureInfo.GetCultureInfo(cultureName);
            }

            await _next(context);
        }
    }
}
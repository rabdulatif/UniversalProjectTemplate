﻿using Root.Logics.AutoDI.Enums;

namespace Root.Logics.AutoDI.Attributes
{
    /// <summary>
    /// 
    /// </summary>
    public class ScopedServiceAttribute : ServiceAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        public ScopedServiceAttribute()
        {
            LifeCycle = ServiceLifeCycle.Scoped;
        }
    }
}
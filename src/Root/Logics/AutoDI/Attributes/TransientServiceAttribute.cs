﻿using Root.Logics.AutoDI.Enums;

namespace Root.Logics.AutoDI.Attributes
{
    /// <summary>
    /// 
    /// </summary>
    public class TransientServiceAttribute : ServiceAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        public TransientServiceAttribute()
        {
            LifeCycle = ServiceLifeCycle.Transient;
        }
    }
}
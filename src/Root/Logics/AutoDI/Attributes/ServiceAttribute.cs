﻿using Root.Logics.AutoDI.Enums;
using System;

namespace Root.Logics.AutoDI.Attributes
{
    /// <summary>
    /// 
    /// </summary>
    public class ServiceAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        public ServiceLifeCycle LifeCycle { get; set; }
    }
}
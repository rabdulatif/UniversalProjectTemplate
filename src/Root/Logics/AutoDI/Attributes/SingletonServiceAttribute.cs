﻿using Root.Logics.AutoDI.Enums;

namespace Root.Logics.AutoDI.Attributes
{
    /// <summary>
    /// 
    /// </summary>
    public class SingletonServiceAttribute : ServiceAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        public SingletonServiceAttribute()
        {
            LifeCycle = ServiceLifeCycle.Singleton;
        }
    }
}
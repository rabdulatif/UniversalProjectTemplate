﻿namespace Root.Logics.AutoDI.Enums
{
    /// <summary>
    /// 
    /// </summary>
    public enum ServiceLifeCycle
    {
        /// <summary>
        /// 
        /// </summary>
        Scoped,

        /// <summary>
        /// 
        /// </summary>
        Singleton,

        /// <summary>
        /// 
        /// </summary>
        Transient
    }
}
﻿using Microsoft.Extensions.DependencyInjection;
using Root.Logics.AutoDI.Attributes;
using Root.Logics.AutoDI.Enums;
using System;
using System.Linq;
using System.Reflection;

namespace Root.Logics.AutoDI
{
    /// <summary>
    /// 
    /// </summary>
    public class AutoDIManager
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public static void Register(IServiceCollection services)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            var types = assemblies
                .SelectMany(s => s.GetTypes())
                .Where(w =>
                    w.IsClass &&
                    !w.IsAbstract &&
                    !w.IsSealed &&
                    w.GetCustomAttribute<ServiceAttribute>() != null)
                .ToList();

            types.ForEach(f => RegisterService(f, services));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="services"></param>
        private static void RegisterService(Type type, IServiceCollection services)
        {
            var attr = type.GetCustomAttribute<ServiceAttribute>();

            if (attr.LifeCycle == ServiceLifeCycle.Scoped)
            {
                RegisterScopedService(type, services);
                return;
            }

            if (attr.LifeCycle == ServiceLifeCycle.Singleton)
            {
                RegisterSingletonService(type, services);
                return;
            }

            if (attr.LifeCycle == ServiceLifeCycle.Transient)
            {
                RegisterTransientService(type, services);
                return;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="services"></param>
        private static void RegisterScopedService(Type type, IServiceCollection services)
        {
            var interfaces = type.GetInterfaces().ToList();
            var baseType = type.BaseType;

            interfaces.ForEach(f => services.AddScoped(f, type));

            if (baseType != typeof(object))
                services.AddScoped(baseType, type);

            services.AddScoped(type, type);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="services"></param>
        private static void RegisterSingletonService(Type type, IServiceCollection services)
        {
            var interfaces = type.GetInterfaces().ToList();
            var baseType = type.BaseType;

            interfaces.ForEach(f => services.AddSingleton(f, type));

            if (baseType != typeof(object))
                services.AddSingleton(baseType, type);

            services.AddSingleton(type, type);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="services"></param>
        private static void RegisterTransientService(Type type, IServiceCollection services)
        {
            var interfaces = type.GetInterfaces().ToList();
            var baseType = type.BaseType;

            interfaces.ForEach(f => services.AddTransient(f, type));

            if (baseType != typeof(object))
                services.AddTransient(baseType, type);

            services.AddTransient(type, type);
        }
    }
}
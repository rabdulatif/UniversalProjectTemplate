﻿using Root.Contexts.Postgres;
using Root.Contexts.Postgres.Tables;
using SB.Common.Extensions;
using SB.Common.Logics.Variables;
using SB.Common.Logics.Variables.Interfaces;
using System.Linq;

namespace Root.Logics.Variables
{
    /// <summary>
    /// 
    /// </summary>
    public class BotVariablesService : IVariableService
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly UPContext Context;

        /// <summary>
        /// 
        /// </summary>
        public BotVariablesService()
        {
            Context = new UPContext();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="variable"></param>
        /// <returns></returns>
        public object GetVariableValue(Variable variable)
            => Context.Variables.FirstOrDefault(f => f.Name == variable.Name)?.Value;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="variable"></param>
        /// <param name="value"></param>
        public void SetVariableValue(Variable variable, object value)
        {
            var dbVariable = Context.Variables.FirstOrDefault(f => f.Name == variable.Name);
            if (dbVariable == null)
            {
                dbVariable = new BotVariable();
                dbVariable.Name = variable.Name;
                Context.Variables.Add(dbVariable);
            }

            dbVariable.Value = value.ToSafeString();
            Context.SaveChanges();
        }
    }
}
﻿using SB.Common.Logics.Variables;
using SB.Common.Logics.Variables.Attributes;
using System.ComponentModel;

namespace Root.Logics.Variables
{
    /// <summary>
    /// 
    /// </summary>
    [VariableService(typeof(BotVariablesService))]
    public class BotVariablesContext : BaseVariableContext
    {
        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(-1001259673316)]
        public Variable<long> ExceptionsChannelId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(-1001276354233)]
        public Variable<long> LogChannelId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(-1001265197302)]
        public Variable<long> RequestResponseChannelId { get; set; }
    }
}
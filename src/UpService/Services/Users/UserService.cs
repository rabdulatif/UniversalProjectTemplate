﻿using Root.Logics.AutoDI.Attributes;

namespace UpService.Services.Users
{
    /// <summary>
    /// 
    /// </summary>
    [ScopedService]
    public class UserService : IUserService
    {
        /// <summary>
        /// 
        /// </summary>
        public string DoSomething() => "Do Something test";
    }
}
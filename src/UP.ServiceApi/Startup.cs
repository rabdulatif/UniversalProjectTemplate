using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Root.Contexts.Postgres;
using Root.Extensions;
using Root.Helpers;
using Root.Logics.Variables;
using SB.Common.Logics.Variables.Interfaces;
using SB.TelegramBot;
using System.Reflection;
using UP.ServiceApi.Helpers;

namespace UP.ServiceApi
{
    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.ConfigureJwtAuthService();
            services.AddControllers();

            services.AddPostgresDbContext(AppConfigHelper.GetConnectionString());
            services.AddScoped<UPContext, UPContext>();
            services.AddScoped<BotVariablesContext>();
            services.AddScoped<IVariableService, BotVariablesService>();

            services.AddAutoRegisterServices();
            services.AddApiSwagger(Assembly.GetExecutingAssembly().GetName().Name);
            services.AddApiCors();
            services.AddHttpContextAccessor();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseCors(builder => builder
                    .AllowAnyMethod()
                    .AllowCredentials()
                    .SetIsOriginAllowed((host) => true)
                    .AllowAnyHeader());

            app.UseLanguageMiddleware();
            app.UseRequestResponseLogging();

            app.UseAuthentication();
            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "UP.ServiceApi v1"); c.RoutePrefix = "swagger"; });

            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            if (app.ApplicationServices.GetService<IHttpContextAccessor>() != null)
                HttpContextHelper.Accessor = app.ApplicationServices.GetRequiredService<IHttpContextAccessor>();

            app.UseMigrateDatabase<UPContext>();
            UseTelegramBot();
        }

        /// <summary>
        /// 
        /// </summary>
        public void UseTelegramBot()
        {
            var token = AppConfigHelper.GetConfigValue("BotToken");
            TelegramBotApplication.Run(token);
        }
    }
}
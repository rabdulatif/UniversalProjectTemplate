﻿using Microsoft.Extensions.Configuration;

namespace UP.ServiceApi.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class AppConfigHelper
    {
        /// <summary>
        /// 
        /// </summary>
        private static IConfiguration _hostingConfiguration;

        /// <summary>
        /// 
        /// </summary>
        private static IConfiguration _configuration;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IConfiguration GetHostingConfiguration()
        {
            if (_hostingConfiguration != null)
                return _hostingConfiguration;

            _hostingConfiguration = new ConfigurationBuilder()
                .AddJsonFile("hosting.json", optional: true, reloadOnChange: true)
                .Build();

            return _hostingConfiguration;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IConfiguration GetConfiguration()
        {
            if (_configuration != null)
                return _configuration;

            _configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();

            return _configuration;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetConnectionString() => GetConfigValue("ConnectionString");

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetHosting() => GetHostingValue("host");

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConfigValue(string key) => GetConfiguration().GetValue<string>(key);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetHostingValue(string key) => GetHostingConfiguration().GetValue<string>(key);
    }
}
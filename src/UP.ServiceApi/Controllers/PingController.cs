﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace UP.ServiceApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PingController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string Get() => DateTime.Now.ToShortDateString();
    }
}
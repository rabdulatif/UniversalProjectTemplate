﻿using Microsoft.AspNetCore.Mvc;
using Root.Logics.Variables;
using SB.Common.Logics.Variables.Interfaces;
using System;
using UP.TelegramBot.Models;
using UP.TelegramBot.Services.Telegram;
using UpService.Services.Users;

namespace UP.ServiceApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IUserService Service;

        /// <summary>
        /// 
        /// </summary>
        private readonly ITelegramService TelegramService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        /// <param name="telegramService"></param>
        public UserController(IUserService service, ITelegramService telegramService)
        {
            Service = service;
            TelegramService = telegramService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("getSmth")]
        public string Get() => Service.DoSomething();

        /// <summary>
        /// 
        /// </summary>
        [HttpGet("sendTelegramLog")]
        public void SendTelegramLog() => TelegramService.SendLog(new LogInfo("", null, ""));

        /// <summary>
        /// 
        /// </summary>
        [HttpGet("sendTelegramException")]
        public void SendTelegramException() => TelegramService.SendException(new ExceptionInfo(new Exception("Test error message"), nameof(SendTelegramException), null, ""));

        /// <summary>
        /// 
        /// </summary>
        [HttpGet("sendTelegramRequestResponse")]
        public void SendTelegramRequestResponse() => 
            TelegramService.SendRequestResponse(new RequestResponseInfo("Test request",DateTime.Now,"Test response",DateTime.Now.AddSeconds(2),nameof(SendTelegramRequestResponse),""));
    }
}
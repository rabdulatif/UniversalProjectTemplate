﻿using System;

namespace UP.TelegramBot.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    internal static class ExceptionHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        internal static string TryGetInnerException(this Exception ex)
        {
            try
            {
                return GetInnerException(ex);
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private static string GetInnerException(Exception ex)
        {
            var innerMessage = string.Empty;
            Exception innerEx = null;

            while ((innerEx = ex?.InnerException) != null)
            {
                innerMessage += innerEx.Message + "\n";
            }

            return innerMessage;
        }
    }
}
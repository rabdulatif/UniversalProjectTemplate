﻿using System;

namespace UP.TelegramBot.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ExceptionInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public Exception Exception { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Method { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public object InputData { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ExceptionInfo() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="method"></param>
        /// <param name="inputData"></param>
        /// <param name="requestId"></param>
        public ExceptionInfo(Exception exception, string method, object inputData, string requestId)
        {
            Exception = exception;
            Method = method;
            InputData = inputData;
            RequestId = requestId;
        }
    }
}
﻿namespace UP.TelegramBot.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class LogInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string Method { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public LogInfo() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="method"></param>
        /// <param name="message"></param>
        /// <param name="requestId"></param>
        public LogInfo(string method, string message, string requestId)
        {
            Method = method;
            Message = message;
            RequestId = requestId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="method"></param>
        /// <param name="data"></param>
        /// <param name="requestId"></param>
        public LogInfo(string method, object data, string requestId)
        {
            Method = method;
            Data = data;
            RequestId = requestId;
        }
    }
}
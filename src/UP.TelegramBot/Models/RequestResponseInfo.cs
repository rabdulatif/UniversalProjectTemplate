﻿using System;

namespace UP.TelegramBot.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class RequestResponseInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public object Request { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime RequestTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 
        /// </summary>
        public object Response { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime ResponseTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 
        /// </summary>
        public string Method { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RequestId { get; set; }

        public RequestResponseInfo() { }

        public RequestResponseInfo(object request, DateTime requestTime, object response, DateTime responseTime, string method, string requestId)
        {
            Request = request;
            RequestTime = requestTime;
            Response = response;
            ResponseTime = responseTime;
            Method = method;
            RequestId = requestId;
        }
    }
}
﻿using UP.TelegramBot.Models;

namespace UP.TelegramBot.Services.Telegram
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITelegramService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        void SendLog(LogInfo info);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        void SendException(ExceptionInfo info);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        void SendRequestResponse(RequestResponseInfo info);
    }
}
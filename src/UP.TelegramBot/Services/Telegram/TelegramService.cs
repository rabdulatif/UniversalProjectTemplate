﻿using Newtonsoft.Json;
using Root.Helpers;
using Root.Logics.AutoDI.Attributes;
using Root.Logics.Variables;
using SB.TelegramBot.Logics.TelegramBotClients;
using System;
using Telegram.Bot.Types.Enums;
using UP.TelegramBot.Helpers;
using UP.TelegramBot.Models;

namespace UP.TelegramBot.Services.Telegram
{
    /// <summary>
    /// 
    /// </summary>
    [ScopedService]
    public class TelegramService : ITelegramService
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly BotVariablesContext variablesContext;

        /// <summary>
        /// 
        /// </summary>
        public TelegramService(BotVariablesContext context)
        {
            variablesContext = context;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        public void SendException(ExceptionInfo info)
        {
            if (info == null || info.Exception == null)
                return;

            var channelId = variablesContext.ExceptionsChannelId?.Value ?? 0;

            var content = $"<b>Request ID: #{info.RequestId}</b>\n";
            content += $"<b>Request user data: IP:\t{ClientDeviceHelper.GetIpAndUserAgent("\t")}</b>\n\n";
            content += $"<b>Method:</b> {info.Method}\n";
            content += $"<b>Input data:</b>\n{JsonConvert.SerializeObject(info.InputData, Formatting.Indented)}\n\n";

            content += $"<b>Exception:\nMessage:</b> {info.Exception.Message}\n";
            content += $"<b>Inner exceptions:</b> {info.Exception.TryGetInnerException()}\n";
            content += $"<b>Trace:</b>\n{info.Exception.StackTrace}";

            try
            {
                TelegramBotClientManager.Client.SendTextMessageAsync(channelId, content, ParseMode.Html);
            }
            catch { /* ... */ }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        public void SendLog(LogInfo info)
        {
            var channelId = variablesContext.LogChannelId?.Value ?? 0;

            var content = $"<b>Request ID: #{info.RequestId}</b>\n";
            content += $"<b>Request user data: IP:\t{ClientDeviceHelper.GetIpAndUserAgent("\t")}</b>\n\n";
            content += $"<b>Method:</b> {info.Method}\n";
            content += $"<b>Message:</b> {(info.Message.INOWS() ? JsonConvert.SerializeObject(info.Data, Formatting.Indented) : info.Message)}";

            try
            {
                TelegramBotClientManager.Client.SendTextMessageAsync(channelId, content, ParseMode.Html);
            }
            catch { /* ... */ }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        public void SendRequestResponse(RequestResponseInfo info)
        {
            var channelId = variablesContext.RequestResponseChannelId?.Value ?? 0;

            var content = $"<b>Request ID: #{info.RequestId}</b>\n";
            content += $"<b>Request user data: IP:\t{ClientDeviceHelper.GetIpAndUserAgent("\t")}</b>\n\n";
            content += $"<b>Method:</b> {info.Method}\n";
            content += $"<b>Request time:</b> {info.RequestTime}\n";
            content += $"<b>Request:</b> {JsonConvert.SerializeObject(info.Request, Formatting.Indented)}\n\n";
            content += $"<b>Response time:</b> {info.ResponseTime}\n";
            content += $"<b>Response:</b>\n{JsonConvert.SerializeObject(info.Response, Formatting.Indented)}";

            try
            {
                TelegramBotClientManager.Client.SendTextMessageAsync(channelId, content, ParseMode.Html);
            }
            catch { /* ... */ }
        }
    }
}